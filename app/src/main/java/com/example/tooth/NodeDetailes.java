package com.example.tooth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class NodeDetailes extends ParentActivitiy {
EditText titleED,desDE;
private int reccievedid ;
private  boolean openedasupdate = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_node_detailes);
        titleED=findViewById(R.id.ed_title);
        desDE=findViewById(R.id.ed_discription);

        reccievedid=getIntent().getIntExtra("id",-1);
        if (reccievedid!=-1){

            setTitle(R.string.update_note);
            titleED.setText(getIntent().getStringExtra("title"));
            desDE.setText(getIntent().getStringExtra("desc"));
            Button updatebtn = findViewById(R.id.btn_update);
            updatebtn.setVisibility(View.VISIBLE);
            openedasupdate=true;
        }
        else
        setTitle(R.string.add_note);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (openedasupdate){
            return false;
        }
        else
        {
            MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.save_node_menu,menu);
        return true;
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==R.id.item_save_note)

            saveNode();
            return true;


    }

    private  void saveNode(){
    String  writtentitle = titleED.getText().toString();
    String writtenDesc =desDE.getText().toString();
    if (writtentitle.isEmpty())
        titleED.setError(getString(R.string.required_field));
    else
    {
        ContentValues values =new ContentValues();
        values.put("title",writtentitle);
        values.put("description",writtenDesc);
        Data helper = new Data(this);
       SQLiteDatabase db= helper.getWritableDatabase();
        long id=  db.insert("note",null,values);
        if (id !=-1){

            Toast.makeText(this, R.string.note_saved, Toast.LENGTH_SHORT).show();
            finish();
        }

    }

    }

    public void updateNote(View view) {
        String  writtentitle = titleED.getText().toString();
        String writtenDesc =desDE.getText().toString();
        if (writtentitle.isEmpty())
            titleED.setError(getString(R.string.required_field));
        else {

            ContentValues values= new ContentValues();
            values.put("title",writtentitle);
            values.put("description",writtenDesc);
            Data data=new Data(this);
            SQLiteDatabase db =data.getWritableDatabase();
            String[]args={String.valueOf(reccievedid)};
        int id=    db.update("note",values,"id==?",args);
            if (id!=0){
                Toast.makeText(this, R.string.note_updated, Toast.LENGTH_SHORT).show();
                finish();

            }

        }
    }
}