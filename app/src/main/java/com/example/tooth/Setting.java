package com.example.tooth;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.RadioGroup;

public class Setting extends ParentActivitiy {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        setTitle(R.string.settings);
        RadioGroup radioGroup=findViewById(R.id.rg);
        SharedPreferences pref=getSharedPreferences("setting",MODE_PRIVATE);
        String code= pref.getString("code","en");
        switch (code){
            case "en":
                radioGroup.check(R.id.rb_english);
                break;
            case "ar":
                radioGroup.check(R.id.rb_arabic);
                break;
        }
        radioGroup.setOnCheckedChangeListener((radioGroup1, i) -> {
            switch (i){
                case R.id.rb_arabic:
                    savedlangouge("ar");
                    break;
                case R.id.rb_english:
                    savedlangouge("en");
                    break;
            }
        });

    }
    private  void savedlangouge(String languagecode){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setMessage(R.string.close_app)
                .setPositiveButton(R.string.language_dialog_positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SharedPreferences.Editor editor=
                                getSharedPreferences("setting",MODE_PRIVATE).edit();
                        editor.putString("code",languagecode);
                        editor.apply();
                        finishAffinity();
                    }
                }).setCancelable(false).show();
    }
}