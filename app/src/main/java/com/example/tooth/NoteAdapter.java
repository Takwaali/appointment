package com.example.tooth;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NotViewHolder> {
    private Activity activity;
    private ArrayList<NoteModel> notes;

    public NoteAdapter(Activity activity, ArrayList<NoteModel> notes) {
        this.activity = activity;
        this.notes = notes;
    }

    @NonNull
    @Override
    public NotViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater=activity.getLayoutInflater();
       View view = inflater.inflate(R.layout.list_item,parent,false);
       NotViewHolder holder =new NotViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull NotViewHolder holder, int position) {
    holder.titletv.setText(notes.get(position).getTitle());
    holder.destv.setText(notes.get(position).getDescription());
    holder.cardView.setOnClickListener(view -> {
        Intent i = new Intent(activity,NodeDetailes.class);
        i.putExtra("id",notes.get(position).getId());
        i.putExtra("title",notes.get(position).getTitle());
        i.putExtra("desc",notes.get(position).getDescription());
        activity.startActivity(i);
    });
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public class NotViewHolder extends RecyclerView.ViewHolder {
        private  final TextView titletv,destv;
        private  final CardView cardView;
        public NotViewHolder(@NonNull View itemView) {
            super(itemView);
            titletv=itemView.findViewById(R.id.tv_title);
            destv=itemView.findViewById(R.id.tv_description);
            cardView=itemView.findViewById(R.id.card_view);
        }
    }
}
