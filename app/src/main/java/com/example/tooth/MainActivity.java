package com.example.tooth;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
TextView dr_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(R.string.empty);
        Thread background = new Thread() {
            public void run() {
                try {
                    // Thread will sleep for 5 seconds
                    sleep(5*1000);

                    // After 5 seconds redirect to another intent
                    Intent i=new Intent(getBaseContext(), Login.class);
                    startActivity(i);

                    //Remove activity
                    finish();
                }
                catch (Exception e) {
                }
            }
        };
        // start thread
        background.start();
        dr_name=findViewById(R.id.Dr_name);
        Animation animation= AnimationUtils.loadAnimation(this,R.anim.zoomin);
        dr_name.startAnimation(animation);

    }
}