package com.example.tooth;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Locale;

public class ParentActivitiy extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences pref =getSharedPreferences("setting",MODE_PRIVATE);
      String code=  pref.getString("code","en");
      changelaguage(code);
    }


    private  void changelaguage(String code){
        Locale locale=new Locale(code);
        Resources resources=getResources();
        Configuration configuration = resources.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(locale);
        }
        resources.updateConfiguration(configuration,resources.getDisplayMetrics());
    }
}
