package com.example.tooth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

public class Appointments extends ParentActivitiy {
private ArrayList<NoteModel>notes=new ArrayList<>();
private NoteAdapter adapter;
private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointments);
        setTitle(R.string.all_notes);


    }

    @Override
    protected void onResume() {
        super.onResume();
        getNotes();
    }

    public void openNodeDetailes(View view) {
        Intent i= new Intent(this,NodeDetailes.class);
        startActivity(i);
    }
    private  void  getNotes(){
        notes.clear();
        Data data= new Data (this);
     SQLiteDatabase db= data.getReadableDatabase();
    Cursor cursor= db.rawQuery("SELECT * FROM note ",null)  ;
     while ( cursor.moveToNext())
     {
      int id = cursor.getInt(0);
      String title =cursor.getString(1);
         String des =cursor.getString(2);
         notes.add(new NoteModel(id,title,des));
     }
        listNode();
    }
    private void listNode(){
        View view=findViewById(R.id.no_note);
        if (notes.size()==0)
            view.setVisibility(View.VISIBLE);
        else{
            view.setVisibility(View.INVISIBLE);
            adapter=new NoteAdapter(this,notes);
            recyclerView=findViewById(R.id.recycler_view);
            recyclerView.setAdapter(adapter);
            swipeToDelete();
        }

    }
    private void swipeToDelete(){
        ItemTouchHelper.SimpleCallback callback= new ItemTouchHelper.SimpleCallback(0
                ,ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView,
                                  @NonNull RecyclerView.ViewHolder viewHolder,
                                  @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
             int position =  viewHolder.getAdapterPosition();
            showDeleteDailog(position);
            }
        };
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);
    }
    private  void deleteFromDb(int position){

        Data data= new Data(this);
        SQLiteDatabase db =data.getWritableDatabase();
        String[] arges={ String.valueOf(notes.get(position).getId())};
        int deletedRows=db.delete("note"," id ==?",arges);
        if (deletedRows!=0){
            Toast.makeText(this, R.string.note_deleted, Toast.LENGTH_SHORT).show();
        }
    }
    private void showDeleteDailog(int position ){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle(R.string.delete_dialog_title)
                .setMessage(R.string.delete_dialog_message)
                .setPositiveButton(R.string.delete_dialog_positive, (dialogInterface, i) -> {
                    deleteFromDb(position);
                    notes.remove(position);
                    adapter.notifyDataSetChanged();
                    if (notes.size()==0){
                        View view = findViewById(R.id.no_note);
                        view.setVisibility(View.VISIBLE);
                    }
                })
                .setNegativeButton(R.string.delete_dialog_negative, (dialogInterface, i) -> {
                    adapter.notifyItemChanged(position);
                })
                .setCancelable(false)
                .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater =getMenuInflater();
        inflater.inflate(R.menu.setting_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId()==R.id.item_setting)
        {Intent i = new Intent(this,Setting.class);
        startActivity(i);

        }
        return super.onOptionsItemSelected(item);
    }
}